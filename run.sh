#!/usr/bin/env bash
# -*- coding: utf-8 -*-

# Load in the functions and animations
source ./bash_loading_animations/bash_loading_animations.sh
# Run BLA::stop_loading_animation if the script is interrupted
trap BLA::stop_loading_animation SIGINT

mkdir ~/.uptime-kuma
mkdir ~/.wg-easy
echo "Directory uptime-kuma and wg-easy created successfully."

add_aliases() {
    echo "Add some useful aliases"
    {
        # Add some aliases
        echo "alias les='batcat --plain'"
        echo "alias gs='git status'"
        echo "alias l='ls -lAh --group-directories-first --color=auto'"
        echo "alias ll='ls -lh --group-directories-first --color=auto'"
        echo "alias docker='sudo docker'"
        echo "alias md=mkdir"
    } >>"/home/$USER/.bashrc"
}

# Check if Docker is installed and install if not
docker_install() {
    if ! command -v docker &>/dev/null; then
        curl -fsSL https://get.docker.com | sh
        sudo usermod -aG docker "$(whoami)"
        echo "Docker installed and user added to the Docker group."
    else
        echo "Docker is already installed."
    fi
}

# Function to start Docker service if not already running
start_service() {
    local service_name=$1
    echo "Checking if $service_name service is running..."
    if ! docker compose -f services.yaml ps | grep -q "$service_name"; then
        echo "$service_name service is not running. Starting it..."
        docker compose -f services.yaml up -d "$service_name"
    else
        echo "$service_name service is already running."
    fi
}

# Function to start all Docker services if not already running
start_services() {
    start_service "wireguard"
    start_service "uptimekuma"
    start_service "kanbanboard"
}

# Check and create dependencies
BLA::start_loading_animation "${BLA_modern_metro[@]}"
add_aliases
BLA::stop_loading_animation

BLA::start_loading_animation "${BLA_modern_metro[@]}"
docker_install
BLA::stop_loading_animation

BLA::start_loading_animation "${BLA_modern_metro[@]}"
start_services
BLA::stop_loading_animation
