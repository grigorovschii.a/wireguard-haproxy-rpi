# Repo to host Wireguard and HAProxy for RP3b

This repository contains scripts and configurations for setting up WireGuard VPN and HAProxy on top of Docker.

## Script 1: Install Docker and Configure Default Route

The script `run.sh` checks if Docker is installed and installs it if necessary. Additionally, it creates a default route if it doesn't exist and adds the current user to the Docker group.

## Script 2: Docker Compose Configuration

The `docker-compose.yml` file defines two services:

### 1. WireGuard VPN Server

This service uses the image `ghcr.io/wg-easy/wg-easy` to run a WireGuard VPN server. It is configured with the following environment variables:

- `LANG`: Set to English.
- `WG_HOST`: VPN hostname.
- `PASSWORD`: VPN password.
- `WG_PERSISTENT_KEEPALIVE`: Keep-alive interval for client connections.
- `WG_DEFAULT_DNS`: Default DNS servers.
- `WG_ALLOWED_IPS`: Allowed IP addresses for VPN clients.
- `UI_TRAFFIC_STATS`: Enable traffic statistics in the UI.

It exposes port `51820/udp` for VPN connections and mounts the `~/.wg-easy` directory to `/etc/wireguard` inside the container.

### 2. HAProxy Load Balancer

This service uses the `haproxy:latest` image to run an HAProxy load balancer. It mounts the `./haproxy` directory to `/usr/local/etc/haproxy` inside the container and exposes port `4443/tcp` for HTTPS traffic.

The HAProxy service depends on the WireGuard service and restarts automatically.

---

**Note:** Ensure you have Docker and Docker Compose installed on your system before running these scripts.
